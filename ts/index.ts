import * as plugins from './smartfile-interfaces.plugins';

export interface VirtualDirTransferableObject {
  files: string[];
}